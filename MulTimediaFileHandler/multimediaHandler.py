#! /usr/bin/env python

from sys import argv
from subprocess import call


script, inFile, outFile=argv
print inFile, outFile

print """+++++++++++++++++++++++\nMultimedia Handler.\n+++++++++++++++++++++++\nA simple program using ffmpeg tool"""
print "\nMay I assume your first argument is the source media file on which the operations shall be performed and second one is where and with what name it shall be saved ? (RET for proceeding)"
raw_input()


print "\nOptions :"
print """1.Convert format\n2.Trim\n3.Make GIF\n4.Concatenate Media Files\n\nChoose an option to proceed.."""

choice= int(raw_input("> "))

if(choice==1):
    print "+++++++++++++++++++++++++++++++\nConvert your files quickly\n+++++++++++++++++++++++++++++++\n"
    call(["ffmpeg", "-i", inFile, outFile ])


elif(choice==2):
    print "+++++++++++++++++++++++++++++++\nTrim specific parts from your video/audio\n++++++++++++++++++++++++++++\n"
    fromWhere=raw_input("From time (HH:MM:SS) > ")
    toWhere=raw_input("To time (HH:MM:SS) > ")
    call(["ffmpeg", "-i", inFile, "-ss", fromWhere, "-to", toWhere, "-async" ," 1", "-strict", " -2", outFile])

elif(choice==3):
    print "+++++++++++++++++++++++++++++++\nMake GIF from Video\n+++++++++++++++++++++++++++++++\n"
    chance=raw_input("Make the whole video gif ?(y/n) >")
    if(chance=='y' or chance=='Y'):
        outFileName=raw_input("Name the GIF ( stored in script dir ) : ")
        outFile=str(outFileName)+str(".gif")
        print "%d will be stored in the script directory" %(outFile)
        call(["ffmpeg", "-i", inFile, "-vf", outFile])
    elif(chance=='n' or chance=='N'):
        fromWhere=raw_input("From Time > ")
        toWhere=raw_input("To Time > ")
        outFileName=raw_input("Name the GIF ( stored in script dir ) : ")
        outFile=str(outFileName)+str(".gif")
        print "%r will be stored in the script directory" %(outFile)
        call(["ffmpeg", "-i", inFile, "-vf", "scale=500:-1", "-ss", fromWhere, "-to", toWhere, "-r", "10", outFile])
    else:
        print "Check your input"

elif(choice==4):
    print "+++++++++++++++++++++++++++++++\nConcatenate two videos/audios\n+++++++++++++++++++++++++++++++\n"
    print "Concatenating the two files specified in the argument. Resulting file will be stored in the script directory under the name you specifies\n"
    outPuT=raw_input("Enter the name of concatenated file > ")
    call(["touch", "textCat.txt"])
    filename="textCat.txt"
    textFile=open(filename,'w')
    textFile.write("file ")
    textFile.write("'")
    textFile.write(inFile)
    textFile.write("'")
    textFile.write("\n")
    textFile.write("file ")
    textFile.write("'")
    textFile.write(outFile)
    textFile.write("'")
    textFile.write("\n")
    textFile.close()
    call(["ffmpeg", "-f", "concat", "-i", "textCat.txt", "-c", "copy", outPuT ])
    call(["rm", "textCat.txt"])

else:
    print "Please Check your input"


#end

